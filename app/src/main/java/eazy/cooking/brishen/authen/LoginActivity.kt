package eazy.cooking.brishen.authen

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import eazy.cooking.brishen.MainActivity
import eazy.cooking.brishen.R

import java.lang.Exception

class LoginActivity : AppCompatActivity() {
    //Login Google
    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var mGoogleSignInClient: GoogleSignInClient

    lateinit var ediTextEmail: TextInputEditText
    lateinit var ediTextPassWord: TextInputEditText
    lateinit var buttonLogin: Button
    lateinit var buttonLoginGoogle: Button
    lateinit var textViewResgister: TextView
    private val SECOND_ACTIVITY_REQUEST_CODE = 1
    var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        supportActionBar?.hide()

        initView()
    }

    private fun initView() {
        ediTextEmail = findViewById(R.id.text_input_edit_text_email_signin)
        ediTextPassWord = findViewById(R.id.text_input_edit_text_password_signin)
        buttonLogin = findViewById(R.id.button_sign_in)
        buttonLoginGoogle = findViewById(R.id.button_sign_in_google)
        textViewResgister = findViewById(R.id.text_view_create_account)
        firebaseAuth = FirebaseAuth.getInstance()

        setupView()
        setupViewGoogle()
    }

    //Login email
    private fun setupView() {
        textViewResgister.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivityForResult(intent, SECOND_ACTIVITY_REQUEST_CODE)
        }
        progressDialog = ProgressDialog(this)

        buttonLogin.setOnClickListener {
            val tmpEmail = ediTextEmail.text.toString().trim()
            val tmpPassword = ediTextPassWord.text.toString().trim()

            if (tmpEmail.isNullOrEmpty()) {
                ediTextEmail.error = "Không được để trống"
                ediTextEmail.requestFocus()
                return@setOnClickListener
            } else if (tmpPassword.isNullOrEmpty()) {
                ediTextPassWord.error = "Không được để trống"
                ediTextPassWord.requestFocus()
                return@setOnClickListener
            } else {
                progressDialog!!.show()
                firebaseAuth.signInWithEmailAndPassword(tmpEmail, tmpPassword)
                    .addOnCompleteListener {
                        progressDialog!!.dismiss()
                        if (it.isSuccessful) {
                            val intent = Intent(this, MainActivity::class.java)
                            Toast.makeText(this, "Đăng nhập thành công!!", Toast.LENGTH_LONG).show()
                            startActivity(intent)
                            finish()
                        } else {
                            Toast.makeText(
                                this,
                                "Mật khẩu hoặc Tài khoản không đúng!!",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }
            }

        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // login email
        if (requestCode == RC_SIGN_IN) {
            //sign in google
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
//            val exception = task.exception
            if (task.isSuccessful) {
                try {
                    val account = task.getResult(ApiException::class.java)!!
                    firebaseAuthWithGoogle(account.idToken!!)
                } catch (ex: Exception) {
                    Log.w("LoginActivity", "Google Sign In Failed", ex)
                }
            }
        } else if (requestCode == SECOND_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                val returnEmailString: String? = data!!.getStringExtra("EMAIL")
                val returnPasswordString: String? = data!!.getStringExtra("PASSWORD")

                ediTextEmail.setText(returnEmailString)
                ediTextPassWord.setText(returnPasswordString)
            }
        } else {
            Toast.makeText(this, "Không đăng nhập được", Toast.LENGTH_LONG).show()
        }
    }

    //login google
    private fun firebaseAuthWithGoogle(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        firebaseAuth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Log.d("LoginActivity", "signInWithCredential: failed", task.exception)
                    startActivity(Intent(this, MainActivity::class.java))
                    Toast.makeText(this, "Đăng nhập bằng Google thành công", Toast.LENGTH_LONG).show()
                    finish()
                } else {
                    Log.d("LoginActivity", "signInWithCredential: failed", task.exception)
                }
            }
    }

    //Login Google firebase
    private fun setupViewGoogle() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken("198256044865-r9g9e2p14k1nrd313ra70na90m22cak9.apps.googleusercontent.com")
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        buttonLoginGoogle.setOnClickListener {
            singIn()
        }
    }

    //Login google
    private fun singIn() {
        val signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    companion object {
        private const val RC_SIGN_IN = 200
    }
}