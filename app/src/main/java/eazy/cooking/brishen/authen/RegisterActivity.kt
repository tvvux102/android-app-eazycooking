package eazy.cooking.brishen.authen

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import eazy.cooking.brishen.R


class RegisterActivity : AppCompatActivity() {

    lateinit var editTextName: TextInputEditText
    lateinit var editTextEmail: TextInputEditText
    lateinit var editTextPassWord: TextInputEditText
    lateinit var editTextConfirmPassWord: TextInputEditText
    lateinit var buttonResgister: Button
    lateinit var textViewYes: TextView
    lateinit var firebaseAuth: FirebaseAuth
    var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        supportActionBar?.hide()
        initView()
    }

    private fun initView() {
        editTextName = findViewById(R.id.text_input_edit_text_name_create_accont)
        editTextEmail = findViewById(R.id.text_input_edit_text_email_create_accont)
        editTextPassWord = findViewById(R.id.text_input_edit_text_password_create_accont)
        editTextConfirmPassWord =
            findViewById(R.id.text_input_edit_text_confirm_password_create_accont)
        buttonResgister = findViewById(R.id.button_create_accont)
        textViewYes = findViewById(R.id.text_view_signin_account)

        setupView()
    }

    private fun setupView() {
        progressDialog = ProgressDialog(this)
        firebaseAuth = FirebaseAuth.getInstance()

        textViewYes.setOnClickListener { finish() }

        buttonResgister.setOnClickListener {
            val tmpName = editTextName.text.toString().trim()
            val tmpEmail = editTextEmail.text.toString().trim()
            val tmpPassword = editTextPassWord.text.toString().trim()
            val tmpConfirmPassword = editTextConfirmPassWord.text.toString().trim()

            if (tmpName.isNullOrEmpty()) {
                editTextName.error = "Không được để trống"
                editTextName.requestFocus()
                return@setOnClickListener
            } else if (tmpEmail.isNullOrEmpty()) {
                editTextEmail.error = "Không được để trống"
                editTextEmail.requestFocus()
                return@setOnClickListener
            } else if (tmpPassword.isNullOrEmpty()) {
                editTextPassWord.error = "Không được để trống"
                editTextPassWord.requestFocus()
                return@setOnClickListener
            } else if (tmpPassword.length < 8) {
                editTextPassWord.error = "Mật khẩu phải lớn hơn 8 kí tự!"
                editTextPassWord.requestFocus()
                return@setOnClickListener
            } else if (tmpConfirmPassword.isNullOrEmpty()) {
                editTextConfirmPassWord.error = "Không được để trống"
                editTextConfirmPassWord.requestFocus()
                return@setOnClickListener
            } else if (tmpConfirmPassword != tmpPassword) {
                editTextPassWord.isEnabled = true
                editTextPassWord.error = "Mật khẩu không khớp!"
                editTextConfirmPassWord.isEnabled = true
                editTextConfirmPassWord.error = "Mật khẩu không khớp!"
                return@setOnClickListener
            } else {
                progressDialog!!.show()
                firebaseAuth.createUserWithEmailAndPassword(tmpEmail, tmpPassword)
                    .addOnCompleteListener {
                        progressDialog!!.dismiss()
                        if (it.isSuccessful) {
                            val intent = Intent()
                            val strEmail = editTextEmail.text.toString().trim()
                            val strPassword = editTextPassWord.text.toString().trim()

                            intent.putExtra("EMAIL", strEmail)
                            intent.putExtra("PASSWORD", strPassword)

                            setResult(RESULT_OK, intent)

                            finish()
                        } else {
                            Toast.makeText(this, "Loi dang ki", Toast.LENGTH_LONG).show()
                        }
                    }
            }

        }

    }
}